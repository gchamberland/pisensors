from influxdb import InfluxDBClient
from time import strptime
from time import strftime
from math import isnan


client = InfluxDBClient(host='localhost', port=8086)

client.switch_database('test')

with open('../temp_log.txt', 'rt') as file:
	line = file.readline()
	line = file.readline()

	points = []

	while line:
		fields = line.split(',')

		time = strptime(fields[0], '%d/%m/%Y %H:%M:%S')
		intTemp = float(fields[1])
		intHum = float(fields[2])
		extTemp = float(fields[3])

		if isnan(intTemp):
			intTemp = 0.0

		if isnan(extTemp):
			extTemp = 0.0

		if isnan(intHum):
			intHum = 0.0

		if time > strptime('01/01/2019', '%d/%m/%Y'):
			print(strftime('%d/%m/%Y %H:%M:%S', time), str(intTemp), str(intHum), str(extTemp))
			points.append({
					"measurement" : "houseEvents",
					"time" : strftime('%Y-%m-%dT%H:%M:%SZ', time),
					"fields" : {
						"intTemp" : intTemp,
						"intHum" : intHum,
						"extTemp" : extTemp
					}
				}
			)

		if len(points) > 10000:
			client.write_points(points)
			points.clear()

		line = file.readline()

	client.write_points(points)
	points.clear()
			
