#include <wiringPi.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>
#include <thread>

#include "pi_2_dht_read.h"

#include "influxdb.hpp"

int clockpin = 2;
int mosipin = 0;
int misopin = 1;
int cspin = 3;

// read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
int readADC(int adcnum) {

  if ((adcnum > 7) || (adcnum < 0)) return -1; // Wrong adc address return -1

  pinMode(cspin, OUTPUT);
  pinMode(clockpin, OUTPUT);
  pinMode(mosipin, OUTPUT);
  pinMode(misopin, INPUT);

  // algo
  digitalWrite(cspin, HIGH);

  digitalWrite(clockpin, LOW); //  # start clock low
  digitalWrite(cspin, LOW); //     # bring CS low

  int commandout = adcnum;
  commandout |= 0x18; //  # start bit + single-ended bit
  commandout <<= 3; //    # we only need to send 5 bits here
 
  for (int i=0; i<5; i++) {
    if (commandout & 0x80) 
      digitalWrite(mosipin, HIGH);
    else   
      digitalWrite(mosipin, LOW);
      
    commandout <<= 1;
    digitalWrite(clockpin, HIGH);
    digitalWrite(clockpin, LOW);

  }

  int adcout = 0;
  // read in one empty bit, one null bit and 10 ADC bits
  for (int i=0; i<12; i++) {
    digitalWrite(clockpin, HIGH);
    digitalWrite(clockpin, LOW);
    adcout <<= 1;
    if (digitalRead(misopin))
      adcout |= 0x1;
  } 
  digitalWrite(cspin, HIGH);

  adcout >>= 1; //      # first bit is 'null' so drop it
  return adcout;
}

std::string getCurrentDate() {
  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];

  time (&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(buffer,sizeof(buffer),"%d/%m/%Y %H:%M:%S", timeinfo);
  
  return std::string(buffer);
}

static double r = 10000.0,
        u = 5.0,
        a = 0.002783150285651425,
        b = -5.770463044863739e-05,
        c = 1.439388507065137e-06;

void readNTC(double *out) {
    int data = readADC(0);

    double ut = data / 1024.0 * u;
    
    double rt = ((ut / u) * r) / (1.0 - (ut / u));
    
    double ln_rt = std::log(rt);
    double temp = 1.0 / (a + b * ln_rt + c * ln_rt * ln_rt * ln_rt);

    *out = temp - 275.25;
}

int main(void) {

  wiringPiSetup();

  float temp_int, humidity_int;
  double temp_ext;
  int err, attempt = 0;

  std::cout << std::setprecision(4);

  influxdb_cpp::server_info si("127.0.0.1", 8086, "test", "", "");

  std::cout << "Logger started" << std::endl;

  while (true) {

    readNTC(&temp_ext);

    do {
      err = pi_2_dht_read(11, 4, &humidity_int, &temp_int);
    } while (err == DHT_ERROR_TIMEOUT || err == DHT_ERROR_CHECKSUM && attempt++ < 15);

    double timestamp = std::chrono::duration_cast<std::chrono::nanoseconds>(
        std::chrono::system_clock::now().time_since_epoch()
    ).count();

    //std::cout << timestamp << "," << temp_int << "," << humidity_int << "," << temp_ext << std::endl;

    influxdb_cpp::builder()
    .meas("houseEvents")
    .field("intTemp", temp_int, 4)
    .field("intHum", humidity_int, 4)
    .field("extTemp", temp_ext, 4)
    .timestamp(timestamp)
    .post_http(si);

    std::this_thread::sleep_for(std::chrono::minutes(5));

  }

  return 0;
}
